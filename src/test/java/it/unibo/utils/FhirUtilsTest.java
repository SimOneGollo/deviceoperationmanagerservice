package it.unibo.utils;

import org.hl7.fhir.r4.model.DateTimeType;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class FhirUtilsTest {

    @Test
    public void test(){
        DateTimeType appEnd = (DateTimeType) DateTimeType.now().setHour(13).setMinute(0);
        DateTimeType procEnd = (DateTimeType)  DateTimeType.now().setHour(13).setMinute(11);

        assertTrue(FhirUtils.dateIsBetweenOffset(appEnd.getValue(),
            procEnd.getValue(), appEnd.getValue(), 1000 * 60 * 30));

    }
}
