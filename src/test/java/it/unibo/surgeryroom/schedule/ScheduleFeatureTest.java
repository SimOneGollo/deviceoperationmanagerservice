package it.unibo.surgeryroom.schedule;

import org.eclipse.ditto.json.JsonObject;
import org.eclipse.ditto.model.things.Feature;
import org.eclipse.ditto.model.things.FeatureDefinition;
import org.hl7.fhir.r4.model.DateTimeType;
import org.hl7.fhir.r4.model.Reference;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.time.OffsetDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ScheduleFeatureTest {

    private static final String SCHEDULE_DEF = "vorto.private.simonegollo.it.unibo:Schedule:1.0.0";
    private static final String SCHEDULE_REF_1 = "Schedule/1";
    private static final String SCHEDULE_REF_2 = "Schedule/2";
    private static final String SLOT_REF_1 = "Slot/1";

    SlotEntity slot;
    Feature f;
    Feature f2;

    @BeforeAll
    void init() {

        slot = new SlotEntity(new Reference(SLOT_REF_1));

        f = Feature.newBuilder()
                .definition(FeatureDefinition.fromIdentifier(SCHEDULE_DEF))
                .properties(JsonObject.newBuilder().set(ScheduleFeature.PROPERTY_REF, SCHEDULE_REF_1).build())
                .withId(ScheduleFeature.NAME)
                .build();

        f2 = Feature.newBuilder()
                .definition(FeatureDefinition.fromIdentifier(SCHEDULE_DEF))
                .properties(JsonObject.newBuilder()
                        .set(ScheduleFeature.PROPERTY_REF, SCHEDULE_REF_2)
                        .set(ScheduleFeature.PROPERTY_NOW, slot.toJsonObject())
                        .build())
                .withId(ScheduleFeature.NAME)
                .build();
    }

    @Test
    void generalSlotTest() {
        DateTimeType now = DateTimeType.now();

        assertTrue(slot.getStart().isEmpty());
        assertTrue(slot.getEnd().isEmpty());
        assertTrue(slot.getActivityReference().isEmpty());
        assertTrue(slot.getComment().isEmpty());
        assertEquals(SLOT_REF_1, slot.getReference().getReference());

        slot.setStart(now.getValue());
        slot.setEnd(now.getValue());

        assertTrue(slot.getStart().isPresent());
        assertTrue(slot.getEnd().isPresent());

        assertEquals(now, slot.getStart().get());
        assertEquals(now, slot.getEnd().get());

        String ciao = "ciao";
        slot.setComment(ciao);
        assertTrue(slot.getComment().isPresent());
        assertEquals(ciao, slot.getComment().get());

        slot.setStatus(ciao);
        assertTrue(slot.getStatus().isPresent());
        assertEquals(ciao, slot.getStatus().get());

    }


    @Test
    void scheduleWithOnlyReference() {
        ScheduleFeature sf = ScheduleFeature.fromFeature(f);
        assertEquals(SCHEDULE_REF_1, sf.getReference().getReference());
    }

    @Test
    void scheduleWithRefAndSlot() {
        ScheduleFeature sf = ScheduleFeature.fromFeature(f2);
        assertEquals(SCHEDULE_REF_2, sf.getReference().getReference());
        assertEquals(SLOT_REF_1, sf.getNow().get().getReference().getReference());
    }

}
