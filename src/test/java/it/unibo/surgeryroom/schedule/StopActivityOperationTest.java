package it.unibo.surgeryroom.schedule;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import it.unibo.BaseService;
import it.unibo.MainService;
import it.unibo.surgeryroom.info.RoomInfoFeature;
import it.unibo.surgeryroom.schedule.operation.StartActivityOperation;
import it.unibo.surgeryroom.schedule.operation.StopActivityOperation;
import org.eclipse.ditto.client.DittoClient;
import org.eclipse.ditto.model.base.common.HttpStatusCode;
import org.eclipse.ditto.model.things.ThingId;
import org.hl7.fhir.r4.model.DateTimeType;
import org.hl7.fhir.r4.model.Reference;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class StopActivityOperationTest {

    private static final String ROOM_ID_REF = "Location/1";

    DittoClient client;
    ThingId roomId;
    FhirContext ctx;
    IGenericClient fhirClient;

    Reference roomRef;

    @BeforeAll
    void init() {
        MainService service = new MainService();
        client = service.newClient();
        ctx = FhirContext.forR4();
        fhirClient = ctx.newRestfulGenericClient(BaseService.CONFIG_PROPERTIES.getFhirEndpointOrThrow());
        roomId = ThingId.of(BaseService.CONFIG_PROPERTIES.getNamespaceOrThrow(),
            BaseService.CONFIG_PROPERTIES.getRoomIdOrThrow().get(0));
        roomRef = new Reference("Location/" + roomId.getName());
        //serviceStart(service);
    }

    @Test
    void stopActivity() {
        client.live().message()
            .to(roomId)
            .featureId(ScheduleFeature.NAME)
            .subject(StopActivityOperation.SUBJECT)
            .send((message, err) -> {
                assertTrue(message.getStatusCode().isPresent());
                assertEquals(HttpStatusCode.ACCEPTED, message.getStatusCode().get());
            });
//        try {
//            //elaboration waiting
//            Thread.sleep(5000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
    }

    /**
     * ATTENZIONE:
     * Consigiato initializzare il main service esternamente dai test
     * */
    private void serviceStart(MainService service){
        service.start();
        try {
            //elaboration waiting
            Thread.sleep(20000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
