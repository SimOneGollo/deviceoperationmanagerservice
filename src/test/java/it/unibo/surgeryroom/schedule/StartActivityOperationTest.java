package it.unibo.surgeryroom.schedule;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import it.unibo.BaseService;
import it.unibo.MainService;
import it.unibo.model.DataFactory;
import it.unibo.model.customconcept.LocationOperationalStatus;
import it.unibo.surgeryroom.info.RoomInfoFeature;
import it.unibo.surgeryroom.schedule.operation.StartActivityOperation;
import it.unibo.surgeryroom.surgeryinfo.SurgeryInfoFeature;
import it.unibo.utils.FhirUtils;
import it.unibo.utils.ResourceLoader;
import org.eclipse.ditto.client.DittoClient;
import org.eclipse.ditto.json.JsonObject;
import org.eclipse.ditto.model.base.common.HttpStatusCode;
import org.eclipse.ditto.model.things.Feature;
import org.eclipse.ditto.model.things.Thing;
import org.eclipse.ditto.model.things.ThingId;
import org.hl7.fhir.r4.model.*;
import org.hl7.fhir.r4.model.codesystems.EventStatus;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class StartActivityOperationTest {

    DittoClient client;
    ThingId roomId;
    FhirContext ctx;
    IGenericClient fhirClient;

    Reference roomRef;

    @BeforeAll
    void init() {
        MainService service = new MainService();
        client = service.newClient();
        ctx = FhirContext.forR4();
        fhirClient = ctx.newRestfulGenericClient(BaseService.CONFIG_PROPERTIES.getFhirEndpointOrThrow());
        roomId = ThingId.of(BaseService.CONFIG_PROPERTIES.getNamespaceOrThrow(),
            BaseService.CONFIG_PROPERTIES.getRoomIdOrThrow().get(0));
        roomRef = new Reference("Location/" + roomId.getName());
        //serviceStart(service);
    }

    /**
     * ATTENZIONE:
     * Consigiato initializzare il main service esternamente dai test
     * */
    private void serviceStart(MainService service){
        service.start();
        try {
            //elaboration waiting
            Thread.sleep(20000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    void startActivity() {
        client.live().message()
            .to(roomId)
            .featureId(ScheduleFeature.NAME)
            .subject(StartActivityOperation.SUBJECT)
            .send((message, err) -> {
                assertTrue(message.getStatusCode().isPresent());
                assertEquals(HttpStatusCode.ACCEPTED, message.getStatusCode().get());
            });
        try {
            //elaboration waiting
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    void checkProcedureAfterStart() throws InterruptedException, ExecutionException, TimeoutException {
        Thing thing = client.twin().forId(roomId).retrieve().get(10, TimeUnit.SECONDS);
        Feature sf = thing.getFeatures().get().getFeature(SurgeryInfoFeature.NAME).get();
        SurgeryInfoFeature surgeryInfoFeature = SurgeryInfoFeature.fromFeature(sf);

        assertEquals(surgeryInfoFeature.getStatus(), Procedure.ProcedureStatus.INPROGRESS.toCode());

        Procedure p = fhirClient.read()
            .resource(Procedure.class)
            .withId(surgeryInfoFeature.getReference().get().getReference())
            .encodedJson()
            .execute();

        assertNotNull(p);

        assertNotNull(((Period) p.getPerformed()).getStart());
    }

    @Test
    void checkRoomAfterStart() throws InterruptedException, ExecutionException, TimeoutException {
        Thing thing = client.twin().forId(roomId).retrieve().get(10, TimeUnit.SECONDS);
        Feature rf = thing.getFeatures().get().getFeature(RoomInfoFeature.NAME).get();
        RoomInfoFeature roomInfoFeature = RoomInfoFeature.fromFeature(rf);

        assertEquals(roomInfoFeature.getOperationalStatus().get(), LocationOperationalStatus.OCCUPIED.toCode());
    }
}
