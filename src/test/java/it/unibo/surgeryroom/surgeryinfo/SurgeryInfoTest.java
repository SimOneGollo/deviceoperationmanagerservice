package it.unibo.surgeryroom.surgeryinfo;

import it.unibo.model.customconcept.SurgeryAbortReason;
import it.unibo.surgeryroom.info.IndexEntity;
import it.unibo.surgeryroom.info.RoomInfoFeature;
import it.unibo.surgeryroom.schedule.ScheduleFeature;
import org.eclipse.ditto.model.things.Feature;
import org.eclipse.ditto.model.things.FeatureDefinition;
import org.hl7.fhir.r4.model.Procedure;
import org.hl7.fhir.r4.model.Reference;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class SurgeryInfoTest {

    private static final String SURGERY_DEF ="vorto.private.simonegollo.it.unibo:SurgeryInfo:1.0.0";
    private static final String PROCEDURE_REF_1 = "Procedure/1";
    private static final String LOCATION_NAME = "Sala1";
    private static final float WELL_I_VAL = 0.5f;
    private static final float POST_I_VAL = 0.2f;

    Feature f;
    Feature f2;



    @BeforeAll
    void init() {
        SurgeryInfoFeature baseSurgery =  new SurgeryInfoFeature(Procedure.ProcedureStatus.UNKNOWN.toCode());

        f = Feature.newBuilder()
                .definition(FeatureDefinition.fromIdentifier(SURGERY_DEF))
                .properties(baseSurgery.toJsonObject())
                .withId(SurgeryInfoFeature.NAME)
                .build();

        baseSurgery.setReference(new Reference(PROCEDURE_REF_1));
        baseSurgery.setStatus(Procedure.ProcedureStatus.STOPPED.toCode());
        baseSurgery.setStatusReason(SurgeryAbortReason.NO_INTENSIVE_BED.toCode());
        f2 = Feature.newBuilder()
                .definition(FeatureDefinition.fromIdentifier(SURGERY_DEF))
                .properties(baseSurgery.toJsonObject())
                .withId(SurgeryInfoFeature.NAME)
                .build();
    }

    @Test
    void generalProcedureTest() {
        SurgeryInfoFeature rf = SurgeryInfoFeature.fromFeature(f);
        assertEquals(Procedure.ProcedureStatus.UNKNOWN.toCode(), rf.getStatus());
    }

    @Test
    void ProcedureTest() {
        SurgeryInfoFeature rf = SurgeryInfoFeature.fromFeature(f2);

        assertEquals(Procedure.ProcedureStatus.STOPPED.toCode(), rf.getStatus());
        assertTrue(rf.getStatusReason().isPresent());
        assertEquals(SurgeryAbortReason.NO_INTENSIVE_BED.toCode(), rf.getStatusReason().get());
        assertTrue(rf.getReference().isPresent());
        assertEquals(PROCEDURE_REF_1, rf.getReference().get().getReference());
    }

}
