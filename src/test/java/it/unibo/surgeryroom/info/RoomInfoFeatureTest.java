package it.unibo.surgeryroom.info;

import it.unibo.surgeryroom.schedule.ScheduleFeature;
import org.eclipse.ditto.json.JsonObject;
import org.eclipse.ditto.model.things.Feature;
import org.eclipse.ditto.model.things.FeatureDefinition;
import org.hl7.fhir.r4.model.Reference;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class RoomInfoFeatureTest {

    private static final String ROOM_DEF ="vorto.private.simonegollo.it.unibo:SurgeryRoomInfo:1.0.0";
    private static final String LOCATION_REF_1 = "Location/1";
    private static final String LOCATION_NAME = "Sala1";
    private static final float WELL_I_VAL = 0.5f;
    private static final float POST_I_VAL = 0.2f;

    Feature f;
    Feature f2;



    @BeforeAll
    void init() {
        RoomInfoFeature baseLoctaion =  new RoomInfoFeature(new Reference(LOCATION_REF_1), LOCATION_NAME);

        f = Feature.newBuilder()
                .definition(FeatureDefinition.fromIdentifier(ROOM_DEF))
                .properties(baseLoctaion.toJsonObject())
                .withId(ScheduleFeature.NAME)
                .build();

        baseLoctaion.setWellAllocatedIndex(new IndexEntity(WELL_I_VAL));
        baseLoctaion.setPostponedIndex(new IndexEntity(POST_I_VAL));

        f2 = Feature.newBuilder()
                .definition(FeatureDefinition.fromIdentifier(ROOM_DEF))
                .properties(baseLoctaion.toJsonObject())
                .withId(ScheduleFeature.NAME)
                .build();
    }

    @Test
    void generalLocationTest() {
        RoomInfoFeature rf = RoomInfoFeature.fromFeature(f);

        assertEquals(LOCATION_REF_1, rf.getReference().getReference());
        assertEquals(LOCATION_NAME, rf.getName());
    }

    @Test
    void roomIndex() {
        RoomInfoFeature rf = RoomInfoFeature.fromFeature(f2);

        assertEquals(LOCATION_REF_1, rf.getReference().getReference());
        assertEquals(LOCATION_NAME, rf.getName());

        assertTrue(rf.getWellAllocatedIndex().isPresent());
        assertTrue(rf.getPostponedIndex().isPresent());
        assertEquals(WELL_I_VAL, rf.getWellAllocatedIndex().get().getValue());
        assertEquals(POST_I_VAL, rf.getPostponedIndex().get().getValue());
    }

}
