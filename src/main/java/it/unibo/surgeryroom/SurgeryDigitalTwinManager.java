package it.unibo.surgeryroom;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import it.unibo.BaseDigitalTwinManager;
import it.unibo.model.DataLoader;
import it.unibo.model.customconcept.SurgeryAbortReason;
import it.unibo.surgeryroom.info.RoomInfoFeature;
import it.unibo.surgeryroom.info.index.LateIndexStrategy;
import it.unibo.surgeryroom.info.index.PostponedIndexStrategy;
import it.unibo.surgeryroom.info.index.WellAllocatedIndexStrategy;
import org.eclipse.ditto.client.DittoClient;
import org.eclipse.ditto.model.things.Thing;
import org.eclipse.ditto.model.things.ThingId;
import org.hl7.fhir.r4.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

public class SurgeryDigitalTwinManager extends BaseDigitalTwinManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(SurgeryDigitalTwinManager.class);
    private static final String DT_MODEL = "SurgeryModel.json";

    private final List<Operation> activeOperations = new ArrayList<>();
    private final FhirContext fhirCTX;
    private final IGenericClient fhirClient;

    private Timer schedTimer;

    public SurgeryDigitalTwinManager(final DittoClient client, final IGenericClient fhirClient) {
        super(client);
        this.fhirClient = fhirClient;
        fhirCTX = fhirClient.getFhirContext();
    }

    @Override
    public void createThing(final ThingId id) {
        client.twin().retrieve(id).thenAccept(thing -> {
            if (thing.isEmpty()) {
                super.createThing(id);
                this.createThing(id);
            } else {
                LOGGER.info("in updating");
                Thing updated = DataLoader.loadThing(id, DT_MODEL, fhirCTX.newRestfulGenericClient(fhirClient.getServerBase()));
                client.twin().update(updated).thenAccept((t) -> {
                    LOGGER.info("Thing is Updated");
                    initializeScheduler(id);
                    initializeIndex(id);
                });
            }
        });
    }

    @Override
    public BaseDigitalTwinManager handleOperations(final ThingId id,
                                                   final List<Class<? extends Operation>> operations) {
        for (Class<? extends Operation> op : operations) {
            try {
                LOGGER.info(String.valueOf(op.getDeclaredConstructors().length));
                Constructor constructor = op.getDeclaredConstructors()[0];
                Operation opInstance = (Operation) constructor.newInstance(client,
                    fhirCTX.newRestfulGenericClient(fhirClient.getServerBase())
                );
                activeOperations.add(opInstance);
                opInstance.handle(id);
            } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
                | InvocationTargetException | SecurityException e) {
                LOGGER.error("Operation class not found" + e.toString());
                e.printStackTrace();
            }
        }
        return this;
    }

    private void initializeScheduler(ThingId id){
        DateTimeType now = DateTimeType.now();
        DateTimeType next = now.copy();
        next.setHour(now.getHour() + 1).setMinute(0).setSecond(0);
        long startDelay =  next.getValue().getTime() - now.getValue().getTime();

        new Thread(new ActivityScheduler(fhirClient.getFhirContext(), client, id)).start();

        ActivityScheduler slotScheduler = new ActivityScheduler(fhirClient.getFhirContext(), client, id);
        schedTimer = new Timer("SlotScheduler-" + id.toString(), true);
        schedTimer.scheduleAtFixedRate(slotScheduler, startDelay, 1000 * 60 * 60);
    }

    private void initializeIndex(ThingId id){
        IGenericClient indexInitFhirClient = fhirClient.getFhirContext()
            .newRestfulGenericClient(fhirClient.getServerBase());

        client.twin().forId(id).forFeature(RoomInfoFeature.NAME).retrieve().thenAccept(roomFeature -> {
            RoomInfoFeature roomInfo = RoomInfoFeature.fromFeature(roomFeature);

            IndexStrategy lateIndex = new LateIndexStrategy(indexInitFhirClient, roomInfo.getReference());
            roomInfo.setLateIndex(lateIndex.calculate());

            IndexStrategy wellIndex = new WellAllocatedIndexStrategy(indexInitFhirClient, roomInfo.getReference());
            roomInfo.setWellAllocatedIndex(wellIndex.calculate());

            IndexStrategy postIndex = new PostponedIndexStrategy(indexInitFhirClient, roomInfo.getReference(),
                SurgeryAbortReason.NOT_SPECIFIED);
            roomInfo.setPostponedIndex(postIndex.calculate());

            IndexStrategy postBedIndex = new PostponedIndexStrategy(indexInitFhirClient,
                roomInfo.getReference(), SurgeryAbortReason.NO_INTENSIVE_BED);
            roomInfo.setPostponedBedIndex(postBedIndex.calculate());

            client.twin().forId(id).forFeature(RoomInfoFeature.NAME).setProperties(roomInfo.toJsonObject())
                .thenAccept(v -> LOGGER.info("Index updated"));
        });
    }


}
