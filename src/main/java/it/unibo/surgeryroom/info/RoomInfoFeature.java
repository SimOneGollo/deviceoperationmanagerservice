package it.unibo.surgeryroom.info;

import it.unibo.JsonConvertible;
import org.eclipse.ditto.json.JsonFactory;
import org.eclipse.ditto.json.JsonObject;
import org.eclipse.ditto.json.JsonObjectBuilder;
import org.eclipse.ditto.json.JsonPointer;
import org.eclipse.ditto.model.things.Feature;
import org.hl7.fhir.r4.model.Reference;

import java.util.Optional;

public final class RoomInfoFeature implements JsonConvertible {

    public static final String DEFINITION = "vorto.private.simonegollo.it.unibo:SurgeryRoomInfo:1.0.0";
    public static final String NAME = "roomInfo";

    public static final String PROPERTY_REF = "/status/reference";
    public static final String PROPERTY_NAME = "/status/name";
    public static final String PROPERTY_STATUS = "/configuration/operationalStatus";
    public static final String PROPERTY_INDEX_WELL = "/configuration/wellAllocatedIndex";
    public static final String PROPERTY_INDEX_POST = "/configuration/postponedIndex";
    public static final String PROPERTY_INDEX_POST_BED = "/configuration/postponedBedIndex";
    public static final String PROPERTY_INDEX_LATE = "/configuration/lateIndex";

    private final String name;
    private final Reference reference;
    private String operationalStatus = "";

    private IndexEntity wellAllocatedIndex;
    private IndexEntity postponedIndex;
    private IndexEntity postponedBedIndex;
    private IndexEntity lateIndex;

    public RoomInfoFeature(final Reference reference, final String name) {
        this.name = name;
        this.reference = reference;
    }

    public static RoomInfoFeature fromFeature(final Feature f) {
        String name = f.getProperty(PROPERTY_NAME).get().asString();
        Reference reference = new Reference(f.getProperty(PROPERTY_REF).get().asString());

        RoomInfoFeature info = new RoomInfoFeature(reference, name);

        if (f.getProperty(PROPERTY_STATUS).isPresent()) {
            info.setOperationalStatus(f.getProperty(PROPERTY_STATUS).get().asString());
        }

        if (f.getProperty(PROPERTY_INDEX_WELL).isPresent()) {
            IndexEntity i = IndexEntity.fromJsonObject(f.getProperty(PROPERTY_INDEX_WELL).get().asObject());
            info.setWellAllocatedIndex(i);
        }
        if (f.getProperty(PROPERTY_INDEX_POST).isPresent()) {
            IndexEntity i = IndexEntity.fromJsonObject(f.getProperty(PROPERTY_INDEX_POST).get().asObject());
            info.setPostponedIndex(i);
        }
        if (f.getProperty(PROPERTY_INDEX_POST_BED).isPresent()) {
            IndexEntity i = IndexEntity.fromJsonObject(f.getProperty(PROPERTY_INDEX_POST_BED).get().asObject());
            info.setPostponedBedIndex(i);
        }
        if (f.getProperty(PROPERTY_INDEX_LATE).isPresent()) {
            IndexEntity i = IndexEntity.fromJsonObject(f.getProperty(PROPERTY_INDEX_LATE).get().asObject());
            info.setLateIndex(i);
        }
        return info;
    }

    public String getName() {
        return name;
    }

    public Reference getReference() {
        return reference;
    }

    public Optional<String> getOperationalStatus() {
        return operationalStatus.equals("") ? Optional.empty() : Optional.of(operationalStatus);
    }

    public void setOperationalStatus(final String status) {
        this.operationalStatus = status;
    }

    public Optional<IndexEntity> getWellAllocatedIndex() {
        return wellAllocatedIndex == null ? Optional.empty() : Optional.of(wellAllocatedIndex);
    }

    public void setWellAllocatedIndex(final IndexEntity wellAllocatedIndex) {
        this.wellAllocatedIndex = wellAllocatedIndex;
    }

    public Optional<IndexEntity> getPostponedIndex() {
        return postponedIndex == null ? Optional.empty() : Optional.of(postponedIndex);
    }

    public void setPostponedIndex(final IndexEntity postponedIndex) {
        this.postponedIndex = postponedIndex;
    }

    public Optional<IndexEntity> getPostponedBedIndex() {
        return postponedBedIndex == null ? Optional.empty() : Optional.of(postponedBedIndex);
    }

    public void setPostponedBedIndex(final IndexEntity postponedBedIndex) {
        this.postponedBedIndex = postponedBedIndex;
    }

    public Optional<IndexEntity> getLateIndex() {
        return lateIndex == null ? Optional.empty() : Optional.of(lateIndex);
    }

    public void setLateIndex(final IndexEntity lateIndex) {
        this.lateIndex = lateIndex;
    }

    public JsonObject toJsonObject() {
        JsonObjectBuilder builder = JsonFactory.newObjectBuilder()
                .set(PROPERTY_NAME, getName())
                .set(PROPERTY_REF, getReference().getReference());

        if (getOperationalStatus().isPresent()) {
           builder.set(PROPERTY_STATUS, getOperationalStatus().get());
        }

        if (getWellAllocatedIndex().isPresent()) {
            builder.set(PROPERTY_INDEX_WELL, getWellAllocatedIndex().get().toJsonObject());
        }

        if (getPostponedIndex().isPresent()) {
            builder.set(PROPERTY_INDEX_POST, getPostponedIndex().get().toJsonObject());
        }

        if (getPostponedBedIndex().isPresent()) {
            builder.set(PROPERTY_INDEX_POST_BED, getPostponedBedIndex().get().toJsonObject());
        }

        if (getLateIndex().isPresent()) {
            builder.set(PROPERTY_INDEX_LATE, getLateIndex().get().toJsonObject());
        }

        return builder.build();
    }

}
