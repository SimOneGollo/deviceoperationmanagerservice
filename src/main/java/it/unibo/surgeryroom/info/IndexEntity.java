package it.unibo.surgeryroom.info;


import it.unibo.JsonConvertible;
import it.unibo.surgeryroom.IndexStrategy;
import org.eclipse.ditto.json.JsonFactory;
import org.eclipse.ditto.json.JsonObject;
import org.eclipse.ditto.json.JsonObjectBuilder;

import java.time.OffsetDateTime;
import java.util.Optional;

public final class IndexEntity implements JsonConvertible {

    public static final String FIELD_VALUE = "value";
    public static final String FIELD_TIMESTAMP = "timestamp";
    public static final String FIELD_DESCRIPTION = "description";

    private float value;
    private OffsetDateTime timestamp;
    private String description = "";

    public IndexEntity(final float value) {
        this(value, OffsetDateTime.now());
    }

    public IndexEntity(final float value, final OffsetDateTime timestamp) {
        this.value = value;
        this.timestamp = timestamp;
    }

    public static IndexEntity fromJsonObject(final JsonObject index) {
        float value = (float) index.getValue(FIELD_VALUE).get().asDouble();
        OffsetDateTime timestamp = OffsetDateTime.parse(index.getValue(FIELD_TIMESTAMP).get().asString());

        IndexEntity i = new IndexEntity(value, timestamp);
        if (index.getValue(FIELD_DESCRIPTION).isPresent()) {
            i.setDescription(index.getValue(FIELD_DESCRIPTION).get().asString());
        }

        return i;
    }

    public JsonObject toJsonObject() {
        JsonObjectBuilder builder = JsonFactory.newObject().toBuilder()
                .set(FIELD_VALUE, getValue())
                .set(FIELD_TIMESTAMP, getTimestamp().toString());

        if (getDescription().isPresent()) {
            builder.set(FIELD_DESCRIPTION, getDescription().get());
        }

        return builder.build();
    }

    public OffsetDateTime getTimestamp() {
        return timestamp;
    }

    public float getValue() {
        return value;
    }

    public void setValue(final float value) {
        this.value = value;
        timestamp = OffsetDateTime.now();
    }

    public Optional<String> getDescription() {
        return description.equals("") ? Optional.empty() : Optional.of(description);
    }

    public void setDescription(final String description) {
        this.description = description;
    }
}
