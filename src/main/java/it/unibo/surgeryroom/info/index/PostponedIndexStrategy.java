package it.unibo.surgeryroom.info.index;

import ca.uhn.fhir.rest.client.api.IGenericClient;
import it.unibo.model.DataLoader;
import it.unibo.model.customconcept.SurgeryAbortReason;
import it.unibo.surgeryroom.IndexStrategy;
import it.unibo.surgeryroom.info.IndexEntity;
import org.eclipse.ditto.client.DittoClient;
import org.eclipse.ditto.model.things.ThingId;
import org.hl7.fhir.r4.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Month;
import java.util.List;
import java.util.stream.Collectors;

public final class PostponedIndexStrategy implements IndexStrategy {
    private static final Logger LOGGER = LoggerFactory.getLogger(PostponedIndexStrategy.class);
    private static final String INDEX_DESCRIPTION = "Interventi rinviati il giorno stesso";
    private static final String INDEX_DESCRIPTION_EXT = "Interventi rinviati il giorno stesso, rinvio causato da: ";

    private final IGenericClient fhirClient;
    private final Reference room;
    private final SurgeryAbortReason code;
    private final DataLoader loader;

    public PostponedIndexStrategy(final IGenericClient fhirClient,
                                  final Reference room,
                                  final SurgeryAbortReason code) {
        this.fhirClient = fhirClient;
        this.room = room;
        this.code = code;
        loader = new DataLoader(fhirClient);
    }

    @Override
    public IndexEntity calculate() {
        List<Procedure> procedures = loader.loadProcedureFromFirstUntilToday(room);
        LOGGER.info(String.valueOf(procedures.size()));
        IndexEntity index = new IndexEntity(calculateIndex(procedures));

        index.setDescription(code == SurgeryAbortReason.NOT_SPECIFIED
            ? INDEX_DESCRIPTION
            : INDEX_DESCRIPTION_EXT + code);
        return index;
    }

    private float calculateIndex(final List<Procedure> procedures) {
        int total = procedures.size();
        long n = procedures.stream().filter(procedure -> {
            if (code == SurgeryAbortReason.NO_INTENSIVE_BED) {
                if(procedure.getStatusReason().getCoding().isEmpty()){
                    return false;
                }
                String procedureReasonCode = procedure.getStatusReason().getCoding().get(0).getCode();
                return (procedure.getStatus() == Procedure.ProcedureStatus.NOTDONE)
                    && (code == SurgeryAbortReason.fromCode(procedureReasonCode));
            } else {
                return (procedure.getStatus() == Procedure.ProcedureStatus.NOTDONE);
            }
        }).count();
        return n == 0 ? 0 : (float) n / (float) total;
    }
}
