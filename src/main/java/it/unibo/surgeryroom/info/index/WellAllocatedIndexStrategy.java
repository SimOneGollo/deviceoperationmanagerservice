package it.unibo.surgeryroom.info.index;

import ca.uhn.fhir.rest.client.api.IGenericClient;
import it.unibo.surgeryroom.IndexStrategy;
import it.unibo.surgeryroom.SurgeryDigitalTwinManager;
import it.unibo.surgeryroom.info.IndexEntity;
import it.unibo.utils.FhirUtils;
import org.eclipse.ditto.client.DittoClient;
import org.eclipse.ditto.model.things.ThingId;
import org.hl7.fhir.r4.model.Appointment;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Procedure;
import org.hl7.fhir.r4.model.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public final class WellAllocatedIndexStrategy implements IndexStrategy {

    private static final Logger LOGGER = LoggerFactory.getLogger(WellAllocatedIndexStrategy.class);

    private static final String INDEX_DESCRIPTION = "Sovra-sotto utilizzo delle ore di sala operatoria";
    private static int BUNDLE_MAX_SIZE = 200;

    private final IGenericClient fhirClient;
    private final Reference room;

    public WellAllocatedIndexStrategy(final IGenericClient fhirClient,
                                      final Reference room) {
        this.fhirClient = fhirClient;
        this.room = room;
    }

    @Override
    public IndexEntity calculate() {
        final List<Procedure> procedures = getCompletedProcedure();
        final List<String> ids = procedures.stream()
            .map(be -> FhirUtils.relativeReference(be.getIdElement()).getReference())
            .collect(Collectors.toList());

        final List<Appointment> appointments = getProcedureAppointment(ids);
        long overUnderProcedureCounter = procedures.stream().filter(p -> {
            String procedureRef = FhirUtils.relativeReference(p.getIdElement()).getReference();
            Appointment appointment = appointments.stream()
                .filter(a -> {
                    String appReasonRef = a.getReasonReference().get(0).getReference();
                    return appReasonRef.equals(procedureRef);
                })
                .findFirst().get();

            final long thirtyMin = 1000 * 60 * 30;
            final Date appointmentEnd = appointment.getEnd();
            final Date procedureEnd = p.getPerformedPeriod().getEnd();

            return FhirUtils.dateIsBetweenOffset(appointmentEnd, procedureEnd, appointmentEnd, thirtyMin);
        }).count();

        LOGGER.info(String.valueOf(overUnderProcedureCounter));

        float value = overUnderProcedureCounter == 0 ? 0 : overUnderProcedureCounter / (float) procedures.size();

        final IndexEntity index = new IndexEntity(value);
        index.setDescription(INDEX_DESCRIPTION);
        return index;
    }

    private List<Procedure> getCompletedProcedure() {
        String[] codes = {
            Procedure.ProcedureStatus.COMPLETED.toCode(),
            Procedure.ProcedureStatus.STOPPED.toCode()
        };

        return fhirClient.search()
            .forResource(Procedure.class)
            .where(Procedure.LOCATION.hasId(room.getReference()))
            .and(Procedure.STATUS.exactly().codes(codes))
            .count(BUNDLE_MAX_SIZE)
            .returnBundle(Bundle.class)
            .execute()
            .getEntry()
            .stream()
            .map(be -> (Procedure) be.getResource())
            .collect(Collectors.toList());
    }

    private List<Appointment> getProcedureAppointment(final List<String> ids) {
        return fhirClient.search()
            .forResource(Appointment.class)
            .where(Appointment.REASON_REFERENCE.hasAnyOfIds(ids))
            .returnBundle(Bundle.class)
            .count(ids.size())
            .execute()
            .getEntry()
            .stream()
            .map(be -> (Appointment) be.getResource())
            .collect(Collectors.toList());
    }
}
