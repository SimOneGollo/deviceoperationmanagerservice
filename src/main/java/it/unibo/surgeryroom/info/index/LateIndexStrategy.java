package it.unibo.surgeryroom.info.index;

import ca.uhn.fhir.rest.client.api.IGenericClient;
import it.unibo.model.DataLoader;
import it.unibo.surgeryroom.IndexStrategy;
import it.unibo.surgeryroom.info.IndexEntity;
import it.unibo.surgeryroom.info.RoomInfoFeature;
import org.eclipse.ditto.client.DittoClient;
import org.eclipse.ditto.model.things.ThingId;
import org.hl7.fhir.r4.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Month;
import java.time.OffsetDateTime;
import java.util.Date;
import java.util.List;

public final class LateIndexStrategy implements IndexStrategy {

    private static final Logger LOGGER = LoggerFactory.getLogger(LateIndexStrategy.class);
    private static final String INDEX_DESCRIPTION = "Numero pazienti non presenti in sala operatoria entro le 8.00";

    private final IGenericClient fhirClient;
    private final Reference room;
    private final DataLoader loader;

    public LateIndexStrategy(final IGenericClient fhirClient, final Reference room) {
        this.fhirClient = fhirClient;
        this.room = room;
        loader = new DataLoader(fhirClient);
    }

    @Override
    public IndexEntity calculate() {
        List<Procedure> procedures = loader.loadProcedureFromFirstUntilToday(room);

        long n = procedures.stream()
            .filter(procedure -> procedure.getStatus().equals(Procedure.ProcedureStatus.NOTDONE)
                || procedure.getStatus().equals(Procedure.ProcedureStatus.COMPLETED)
                || procedure.getStatus().equals(Procedure.ProcedureStatus.STOPPED))
            .filter(procedure -> {
                DateTimeType start = procedure.getPerformedPeriod().getStartElement();
                return start.getHour() >= 8;
            }).count();

        float val = n == 0 ? 0 : (float) n / (float) procedures.size();

        IndexEntity index = new IndexEntity(val);
        index.setDescription(INDEX_DESCRIPTION);
        return index;
    }
}
