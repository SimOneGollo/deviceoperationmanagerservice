package it.unibo.surgeryroom.schedule;

import it.unibo.JsonConvertible;
import it.unibo.surgeryroom.schedule.operation.StartActivityOperation;
import org.eclipse.ditto.json.JsonFactory;
import org.eclipse.ditto.json.JsonObject;
import org.eclipse.ditto.json.JsonObjectBuilder;
import org.eclipse.ditto.model.things.Feature;
import org.hl7.fhir.r4.model.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

public final class ScheduleFeature implements JsonConvertible {
    public static final String DEFINITION = "vorto.private.simonegollo.it.unibo:Schedule:1.0.0";
    public static final String NAME = "schedule";
    public static final String PROPERTY_REF = "/configuration/reference";
    public static final String PROPERTY_NOW = "/configuration/now";
    private static final Logger LOGGER = LoggerFactory.getLogger(ScheduleFeature.class);
    private final Reference reference;
    private SlotEntity now;

    public ScheduleFeature(Reference reference) {
        this.reference = reference;
    }

    public static ScheduleFeature fromFeature(Feature feature) {
        Reference ref = new Reference(feature.getProperty(PROPERTY_REF).get().asString());
        ScheduleFeature sf = new ScheduleFeature(ref);
        if (feature.getProperty(PROPERTY_NOW).isPresent()) {
            sf.setNow(SlotEntity.fromJsonObject(feature.getProperty(PROPERTY_NOW).get().asObject()));
        }
        return sf;
    }

    public Reference getReference() {
        return reference;
    }

    public Optional<SlotEntity> getNow() {
        return now == null ? Optional.empty() : Optional.of(now);
    }

    public void setNow(SlotEntity now) {
        this.now = now;
    }

    public JsonObject toJsonObject() {
        JsonObjectBuilder builder = JsonFactory.newObjectBuilder()
            .set(PROPERTY_REF, getReference().getReference());

        if (getNow().isPresent()) {
            builder.set(PROPERTY_NOW, getNow().get().toJsonObject());
        }

        return builder.build();
    }
}

