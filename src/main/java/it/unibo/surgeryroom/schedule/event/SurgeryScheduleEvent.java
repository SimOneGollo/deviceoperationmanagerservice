package it.unibo.surgeryroom.schedule.event;

import org.eclipse.ditto.client.DittoClient;
import org.eclipse.ditto.json.JsonFactory;
import org.eclipse.ditto.model.things.ThingId;

public class SurgeryScheduleEvent {
	
	DittoClient client;
	
	public SurgeryScheduleEvent(DittoClient client) {
		this.client = client;
	}
	
	public SurgeryFirableEvent fire() {
		return new SurgeryFirableEvent(client);
	}
	
	public class SurgeryFirableEvent {
		DittoClient client;
		
		private SurgeryFirableEvent(DittoClient client) {
			this.client = client;
		}
		
		/**
		 * @param id thing that emit event.
		 * @param appointmentRef ref in FHIR format attached as payload.
		 * */
		public void activityScheduledEvent(ThingId id, String appointmentRef) {
			client.live()
				.message()
				.from(id)
				.featureId("schedule")
				.subject("activityScheduled")
				.payload(JsonFactory.readFrom(
					String.format("{\"appointment\": %s}", appointmentRef)
				))
				.contentType("application/json")
				.send();
		}
		
		/**
		 * @param id thing that emit event.
		 * @param activityRef ref in FHIR format of the aborted event.
		 * @param abortReason reason of activity abort.
		 * */
		public void activityAbortedEvent(ThingId id, String activityRef, String abortReason) {
			client.live()
				.message()
				.from(id)
				.featureId("schedule")
				.subject("activityAborted")
				.payload(JsonFactory.readFrom(
					String.format("{"
							+ "  \"reference\": %s,"
							+ "  \"reason\": %s"
							+ "}", activityRef,abortReason)
				))
				.contentType("application/json")
				.send();
		}
		
		/**
		 * @param id thing that emit event.
		 * @param appointmentRef ref in FHIR format attached as payload.
		 * */
		public void activityStartedEvent(ThingId id, String appointmentRef) {
			client.live()
				.message()
				.from(id)
				.featureId("schedule")
				.subject("activityStarted")
				.payload(String.format("{\"appointment\": %s}", appointmentRef))
				.contentType("application/json")
				.send();
		}
		
		/**
		 * @param id thing that emit event.
		 * @param appointmentRef ref in FHIR format attached as payload.
		 * */
		public void activityEndedEvent(ThingId id, String appointmentRef) {
			client.live()
				.message()
				.from(id)
				.featureId("schedule")
				.subject("activityEnded")
				.payload(JsonFactory.readFrom(
						String.format("{\"appointment\": %s}", appointmentRef)
					))
				.contentType("application/json")
				.send();
		}
	}
}
