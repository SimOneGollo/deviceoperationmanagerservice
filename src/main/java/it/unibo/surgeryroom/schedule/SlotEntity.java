package it.unibo.surgeryroom.schedule;

import it.unibo.JsonConvertible;
import it.unibo.utils.FhirUtils;
import org.eclipse.ditto.json.JsonFactory;
import org.eclipse.ditto.json.JsonObject;
import org.eclipse.ditto.json.JsonObjectBuilder;
import org.hl7.fhir.r4.model.DateTimeType;
import org.hl7.fhir.r4.model.Reference;
import org.hl7.fhir.r4.model.Slot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.Optional;

public class SlotEntity implements JsonConvertible {
    public static final String FIELD_REFERENCE = "/reference";
    public static final String FIELD_ACTIVITY_REFERENCE = "/activityReference";
    public static final String FIELD_STATUS = "/status";
    public static final String FIELD_START = "/start";
    public static final String FIELD_END = "/end";
    public static final String FIELD_COMMENT = "/comment";
    private static final Logger LOGGER = LoggerFactory.getLogger(SlotEntity.class);
    private final Reference reference;
    private String status = "";
    private Reference activityReference = null;
    private Date start = null;
    private Date end = null;
    private String comment = "";

    public SlotEntity(final Slot sl) {
        this.reference = FhirUtils.relativeReference(sl.getIdElement());
        this.status = sl.getStatus() == null ? Slot.SlotStatus.FREE.toCode() : sl.getStatus().toCode();
        this.start = sl.getStart();
        this.end = sl.getEnd();
        this.comment = sl.getComment() == null ? "" : sl.getComment();
    }

    public SlotEntity(final Reference reference) {
        this.reference = reference;
    }

    public static SlotEntity fromJsonObject(final JsonObject obj) {
        final Reference reference = new Reference(obj.getValue(FIELD_REFERENCE).get().asString());
        SlotEntity se = new SlotEntity(reference);
        if (obj.getValue(FIELD_STATUS).isPresent()) {
            se.setStatus(obj.getValue(FIELD_STATUS).get().asString());
        }
        if (obj.getValue(FIELD_ACTIVITY_REFERENCE).isPresent()) {
            se.setActivityReference(new Reference(obj.getValue(FIELD_ACTIVITY_REFERENCE).get().asString()));
        }
        if (obj.getValue(FIELD_START).isPresent()) {
            se.setStart(new DateTimeType(obj.getValue(FIELD_START).get().asString()).getValue());
        }
        if (obj.getValue(FIELD_END).isPresent()) {
            se.setEnd(new DateTimeType(obj.getValue(FIELD_END).get().asString()).getValue());
        }
        if (obj.getValue(FIELD_COMMENT).isPresent()) {
            se.setComment(obj.getValue(FIELD_COMMENT).get().asString());
        }
        return se;
    }

    public JsonObject toJsonObject() {
        JsonObjectBuilder builder = JsonFactory.newObject().toBuilder()
            .set(FIELD_REFERENCE, reference.getReference());

        if (getStatus().isPresent()) {
            builder.set(FIELD_STATUS, getStatus().get());
        }
        if (getActivityReference().isPresent()) {
            builder.set(FIELD_ACTIVITY_REFERENCE, getActivityReference().get().getReference());
        }
        if (getStart().isPresent()) {
            DateTimeType start = new DateTimeType(getStart().get());
            builder.set(FIELD_START, start.getValueAsString());
        }
        if (getEnd().isPresent()) {
            DateTimeType end = new DateTimeType(getEnd().get());
            builder.set(FIELD_END, end.getValueAsString());
        }
        if (getComment().isPresent()) {
            builder.set(FIELD_COMMENT, getStatus().get());
        }
        return builder.build();
    }

    public Optional<Date> getStart() {
        return start == null ? Optional.empty() : Optional.of(start);
    }

    public void setStart(final Date start) {
        this.start = start;
    }

    public Optional<Date> getEnd() {
        return end == null ? Optional.empty() : Optional.of(end);
    }

    public void setEnd(final Date end) {
        this.end = end;
    }

    public Reference getReference() {
        return reference;
    }

    public Optional<String> getStatus() {
        return status.equals("") ? Optional.empty() : Optional.of(status);
    }

    public void setStatus(final String status) {
        this.status = status;
    }

    public Optional<Reference> getActivityReference() {
        return activityReference == null ? Optional.empty() : Optional.of(activityReference);
    }

    public void setActivityReference(final Reference activityReference) {
        this.activityReference = activityReference;
    }

    public Optional<String> getComment() {
        return comment.equals("") ? Optional.empty() : Optional.of(comment);
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

}
