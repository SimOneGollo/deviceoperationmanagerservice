package it.unibo.surgeryroom.schedule.operation;

import ca.uhn.fhir.rest.client.api.IGenericClient;
import it.unibo.model.DataLoader;
import it.unibo.model.customconcept.LocationOperationalStatus;
import it.unibo.model.customconcept.SurgeryAbortReason;
import it.unibo.surgeryroom.IndexStrategy;
import it.unibo.surgeryroom.Operation;
import it.unibo.surgeryroom.info.RoomInfoFeature;
import it.unibo.surgeryroom.info.index.LateIndexStrategy;
import it.unibo.surgeryroom.info.index.PostponedIndexStrategy;
import it.unibo.surgeryroom.info.index.WellAllocatedIndexStrategy;
import it.unibo.surgeryroom.schedule.ScheduleFeature;
import it.unibo.surgeryroom.schedule.SlotEntity;
import it.unibo.surgeryroom.schedule.event.SurgeryScheduleEvent;
import it.unibo.surgeryroom.surgeryinfo.SurgeryInfoFeature;
import it.unibo.utils.FhirUtils;
import it.unibo.utils.MessageUtils;
import org.eclipse.ditto.client.DittoClient;
import org.eclipse.ditto.json.JsonObject;
import org.eclipse.ditto.model.base.common.HttpStatusCode;
import org.eclipse.ditto.model.things.*;
import org.hl7.fhir.r4.model.*;
import org.hl7.fhir.r4.model.Appointment.AppointmentStatus;
import org.hl7.fhir.r4.model.Slot.SlotStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class AbortActivityOperation extends Operation {

    public static final String SUBJECT = "abortActivity";

    private static final Logger LOGGER = LoggerFactory.getLogger(AbortActivityOperation.class);
    private static final String ABORT_ACTIVITY_ID = "abort_activity_";
    private final SurgeryScheduleEvent surgeryScheduleEvent;

    public AbortActivityOperation(DittoClient client, IGenericClient fhirClient) {
        super(client, fhirClient);
        surgeryScheduleEvent = new SurgeryScheduleEvent(client);
    }

    @Override
    public boolean handle(ThingId id) {
        LOGGER.info("Start handle: ABORT_ACTIVITY_OPERATION - " + id.getName());
        client.live()
            .forId(id)
            .forFeature(ScheduleFeature.NAME)
            .registerForMessage(ABORT_ACTIVITY_ID + id.getName(), SUBJECT, JsonObject.class,
                message -> {
                    LOGGER.info("Operation message arrived");
                    if (MessageUtils.isValidPayload(message, "reference", "reason")) {
                        final String activityRef = message.getPayload().get().getValue("reference").get().asString();
                        final String abortReason = message.getPayload().get().getValue("reason").get().asString();

                        DataLoader loader = new DataLoader(fhirClient);
                        Appointment appointment = loader.loadAppointment(new Reference(activityRef));
                        String ref = FhirUtils.referenceSplit(appointment.getParticipant().stream()
                            .map(p -> p.getActor().getReference())
                            .filter(pref -> FhirUtils.referenceSplit(pref)[0].equals("Location"))
                            .findFirst()
                            .orElse(""))[1];

                        if (ref.equals(id.getName())) {
                            client.twin().forId(id).retrieve().thenAccept(thing -> {
                                Thing updated = updateTwinInformation(
                                    thing,
                                    appointment,
                                    SurgeryAbortReason.fromCode(abortReason)
                                );
                                if (FhirUtils.dateIsBetween(appointment.getStart(),
                                    DateTimeType.now().getValue(), appointment.getEnd())) {
                                    client.twin().update(updated);
                                }
                                surgeryScheduleEvent.fire().activityAbortedEvent(id, activityRef, abortReason);
                                LOGGER.info("Event fired");
                            });
                            LOGGER.info("Operation accepted");
                            message.reply().statusCode(HttpStatusCode.ACCEPTED).send();
                        } else {
                            message.reply().statusCode(HttpStatusCode.BAD_REQUEST)
                                .payload(MessageUtils.errorResponseMessage(
                                    HttpStatusCode.BAD_REQUEST.toInt(),
                                    HttpStatusCode.BAD_REQUEST.name(),
                                    "appointment handled by digital twin with id: "
                                        + id.getNamespace()
                                        + ":"
                                        + ref,
                                    "This error occur when user try to abort an appointment of another DT",
                                    "")
                                ).send();
                        }
                    } else {
                        LOGGER.error("Wrong operation request parameters");
                        message.reply().statusCode(HttpStatusCode.BAD_REQUEST)
                            .payload(MessageUtils.errorResponseMessage(
                                400,
                                HttpStatusCode.BAD_REQUEST.name(),
                                "some mandatory field not found",
                                "check if exist all mandatory field",
                                "")
                            )
                            .send();
                    }
                });
        return true;
    }

    /**
     * Appointment updating to declare this appointment as cancelled.
     */
    private Thing updateTwinInformation(
        final Thing thing,
        Appointment appointment,
        final SurgeryAbortReason reason) {

        // update appointment in database
        appointment.setStatus(AppointmentStatus.CANCELLED);
        appointment.setSlot(new ArrayList<Reference>());
        fhirClient.update().resource(appointment).execute();
        // update slot in thing and database
        Thing t = updateSlot(thing, appointment);
        // update Procedure in thing and database
        t = updateProcedure(t, appointment, reason);
        // update room status and info thing
        return updateRoomInfo(t);
    }

    /**
     * This function is uset to update all Slot of an appointment to declare id FREE.
     */
    private Thing updateSlot(final Thing thing, final Appointment appointment) {
        Features features = thing.getFeatures().get();
        ScheduleFeature scheduleFeature1 = ScheduleFeature.fromFeature(
            features.getFeature(ScheduleFeature.NAME).get()
        );
        for (Reference ref : appointment.getSlot()) {
            IGenericClient c = fhirClient.getFhirContext().newRestfulGenericClient(fhirClient.getServerBase());
            Slot slot = fhirClient.read().resource(Slot.class).withId(ref.getReference()).execute();
            slot.setStatus(SlotStatus.FREE);
            c.update().resource(slot).execute();
        }
        if (scheduleFeature1.getNow().isPresent()
            && scheduleFeature1.getNow().get().getActivityReference().isPresent()) {

            String activityRef = scheduleFeature1.getNow().get().getActivityReference().get().getReference();
            String appRef = FhirUtils.relativeReference(appointment.getIdElement()).getReference();

            if (activityRef.equals(appRef)) {
                SlotEntity se = scheduleFeature1.getNow().get();
                se.setStatus(SlotStatus.FREE.toCode());
                se.setActivityReference(new Reference());
                scheduleFeature1.setNow(se);
                return thing.setFeatureProperties(ScheduleFeature.NAME, FeatureProperties.newBuilder()
                    .setAll(scheduleFeature1.toJsonObject().stream().collect(Collectors.toList()))
                    .build());
            }
        }
        return thing;
    }

    private Thing updateProcedure(final Thing thing, final Appointment appointment, final SurgeryAbortReason reason) {
        Features features = thing.getFeatures().get();
        SurgeryInfoFeature surgeryInfoFeature = SurgeryInfoFeature.fromFeature(
            features.getFeature(SurgeryInfoFeature.NAME).get()
        );

        surgeryInfoFeature.setStatus(Procedure.ProcedureStatus.NOTDONE.toCode());
        surgeryInfoFeature.setStatusReason(reason.toCode());

        final Reference r = appointment.getReasonReference().get(0);
        final Procedure p = fhirClient.read().resource(Procedure.class).withId(r.getReference()).execute();

        p.setStatus(p.getPerformedPeriod().getStart() == null
            ? Procedure.ProcedureStatus.NOTDONE :
            Procedure.ProcedureStatus.STOPPED);

        p.setStatusReason(reason.toConcept());
        fhirClient.update().resource(p).execute();

        return thing.setFeatureProperties(SurgeryInfoFeature.NAME, FeatureProperties.newBuilder()
            .setAll(surgeryInfoFeature.toJsonObject().stream().collect(Collectors.toList()))
            .build());
    }

    /**
     * Update room info setting operational status to unoccupied and calculating ad updating all index.
     */
    private Thing updateRoomInfo(final Thing thing) {
        Features features = thing.getFeatures().get();
        RoomInfoFeature roomInfo = RoomInfoFeature.fromFeature(features.getFeature(RoomInfoFeature.NAME).get());

        Location loc = fhirClient.read()
            .resource(Location.class)
            .withId(thing.getEntityId().get().getName())
            .encodedJson()
            .execute();
        loc.setOperationalStatus(LocationOperationalStatus.UNOCCUPIED.toConcept().getCodingFirstRep());
        fhirClient.update().resource(loc).execute();

        roomInfo.setOperationalStatus(LocationOperationalStatus.UNOCCUPIED.toCode());

        IndexStrategy lateIndex = new LateIndexStrategy(fhirClient, roomInfo.getReference());
        roomInfo.setLateIndex(lateIndex.calculate());

        IndexStrategy wellIndex = new WellAllocatedIndexStrategy(fhirClient, roomInfo.getReference());
        roomInfo.setWellAllocatedIndex(wellIndex.calculate());

        IndexStrategy postIndex = new PostponedIndexStrategy(fhirClient, roomInfo.getReference(), SurgeryAbortReason.NOT_SPECIFIED);
        roomInfo.setPostponedIndex(postIndex.calculate());

        IndexStrategy postBedIndex = new PostponedIndexStrategy(fhirClient,
            roomInfo.getReference(), SurgeryAbortReason.NO_INTENSIVE_BED);
        roomInfo.setPostponedBedIndex(postBedIndex.calculate());

        return thing.setFeatureProperties(RoomInfoFeature.NAME, FeatureProperties.newBuilder()
            .setAll(roomInfo.toJsonObject().stream().collect(Collectors.toList()))
            .build());
    }
}
