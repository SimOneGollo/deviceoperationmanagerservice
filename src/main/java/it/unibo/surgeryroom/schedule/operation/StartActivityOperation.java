package it.unibo.surgeryroom.schedule.operation;

import ca.uhn.fhir.rest.client.api.IGenericClient;
import it.unibo.model.customconcept.LocationOperationalStatus;
import it.unibo.surgeryroom.Operation;
import it.unibo.surgeryroom.info.RoomInfoFeature;
import it.unibo.surgeryroom.schedule.SlotEntity;
import it.unibo.surgeryroom.surgeryinfo.SurgeryInfoFeature;
import it.unibo.surgeryroom.schedule.ScheduleFeature;
import it.unibo.surgeryroom.schedule.event.SurgeryScheduleEvent;
import org.eclipse.ditto.client.DittoClient;
import org.eclipse.ditto.json.JsonObject;
import org.eclipse.ditto.model.base.common.HttpStatusCode;
import org.eclipse.ditto.model.things.*;
import org.hl7.fhir.r4.model.*;
import org.hl7.fhir.r4.model.codesystems.EventStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.NoSuchElementException;
import java.util.stream.Collectors;


/**
 * take next scheduled surgery and start it
 */
public class StartActivityOperation extends Operation {

    public static final String SUBJECT = "startActivity";

    private static final Logger LOGGER = LoggerFactory.getLogger(StartActivityOperation.class);
    private static final String START_ACTIVITY_ID = "start_activity_";
    private final SurgeryScheduleEvent surgeryScheduleEvent;

    public StartActivityOperation(final DittoClient client, final IGenericClient fhirClient) {
        super(client, fhirClient);
        surgeryScheduleEvent = new SurgeryScheduleEvent(client);
    }

    @Override
    public boolean handle(final ThingId id) {
        LOGGER.info("Start handle: START_ACTIVITY_OPERATION - " + id.getName());
        client.live()
            .forId(id)
            .forFeature(ScheduleFeature.NAME)
            .registerForMessage(START_ACTIVITY_ID + id.getName(), SUBJECT, JsonObject.class,
                message -> {
                    client.twin().forId(id).retrieve().thenAccept(thing -> {
                        Thing updated;
                        Features features = thing.getFeatures().get();
                        updated = updateProcedureStart(
                            thing,
                            features.getFeature(SurgeryInfoFeature.NAME).get(),
                            DateTimeType.now());

                        updated = updateRoomInfo(
                            updated,
                            features.getFeature(RoomInfoFeature.NAME).get()
                        );

                        ScheduleFeature sf = ScheduleFeature.fromFeature(
                            features.getFeature(ScheduleFeature.NAME).get()
                        );
                        SlotEntity slot = sf.getNow().get();
                        LOGGER.info("Twin updating");
                        client.twin().update(updated);

                        surgeryScheduleEvent.fire().activityStartedEvent(id, slot.getReference().getReference());
                        LOGGER.info("Event fired");
                    });
                    message.reply().statusCode(HttpStatusCode.ACCEPTED).send();
                    LOGGER.info("Operation accepted");
                });
        return true;
    }

    /**
     * This function set start date and update the resource in fhir server.
     */
    private Thing updateRoomInfo(final Thing thing, final Feature roomFeature) {
        RoomInfoFeature roomInfoFeature = RoomInfoFeature.fromFeature(roomFeature);
        roomInfoFeature.setOperationalStatus(LocationOperationalStatus.OCCUPIED.toCode());

        Location loc = fhirClient.read()
            .resource(Location.class)
            .withId(thing.getEntityId().get().getName())
            .encodedJson()
            .execute();
        loc.setOperationalStatus(LocationOperationalStatus.OCCUPIED.toConcept().getCodingFirstRep());
        fhirClient.update().resource(loc).execute();

        return thing.setFeatureProperties(RoomInfoFeature.NAME, FeatureProperties.newBuilder()
            .setAll(roomInfoFeature.toJsonObject().stream().collect(Collectors.toList()))
            .build());

    }

    /**
     * This function set start date and update the resource in fhir server.
     */
    private Thing updateProcedureStart(final Thing thing, final Feature surgeryFeature, final DateTimeType start) {
        SurgeryInfoFeature surgery = SurgeryInfoFeature.fromFeature(surgeryFeature);
        surgery.setStatus(Procedure.ProcedureStatus.INPROGRESS.toCode());
        final Reference surgeryReference = surgery.getReference().get();
        final Procedure p = fhirClient.read()
            .resource(Procedure.class)
            .withId(surgeryReference.getReference())
            .execute();
        final Period period = new Period().setStartElement(start);
        p.setPerformed(period);
        p.setStatus(Procedure.ProcedureStatus.INPROGRESS);
        fhirClient.update().resource(p).execute();
        return thing.setFeatureProperties(SurgeryInfoFeature.NAME, FeatureProperties.newBuilder()
            .setAll(surgery.toJsonObject().stream().collect(Collectors.toList()))
            .build());
    }
}
