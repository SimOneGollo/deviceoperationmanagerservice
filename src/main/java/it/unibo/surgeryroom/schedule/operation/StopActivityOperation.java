package it.unibo.surgeryroom.schedule.operation;

import ca.uhn.fhir.rest.client.api.IGenericClient;
import it.unibo.model.customconcept.LocationOperationalStatus;
import it.unibo.model.customconcept.SurgeryAbortReason;
import it.unibo.surgeryroom.IndexStrategy;
import it.unibo.surgeryroom.Operation;
import it.unibo.surgeryroom.info.RoomInfoFeature;
import it.unibo.surgeryroom.info.index.LateIndexStrategy;
import it.unibo.surgeryroom.info.index.PostponedIndexStrategy;
import it.unibo.surgeryroom.info.index.WellAllocatedIndexStrategy;
import it.unibo.surgeryroom.schedule.ScheduleFeature;
import it.unibo.surgeryroom.schedule.SlotEntity;
import it.unibo.surgeryroom.schedule.event.SurgeryScheduleEvent;
import it.unibo.surgeryroom.surgeryinfo.SurgeryInfoFeature;
import org.eclipse.ditto.client.DittoClient;
import org.eclipse.ditto.model.base.common.HttpStatusCode;
import org.eclipse.ditto.model.things.*;
import org.hl7.fhir.r4.model.*;
import org.hl7.fhir.r4.model.codesystems.EventStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.NoSuchElementException;
import java.util.stream.Collectors;

public class StopActivityOperation extends Operation {

    public static final String SUBJECT = "endActivity";
    private static final Logger LOGGER = LoggerFactory.getLogger(StopActivityOperation.class);
    private static final String STOP_ACTIVITY_ID = "stop_activity_";

    private final SurgeryScheduleEvent surgeryScheduleEvent;

    public StopActivityOperation(final DittoClient client, final IGenericClient fhirClient) {
        super(client, fhirClient);
        surgeryScheduleEvent = new SurgeryScheduleEvent(client);
    }

    @Override
    public boolean handle(final ThingId id) {
        LOGGER.info("Start handle: STOP_ACTIVITY_OPERATION - " + id.getName());
        client.live()
            .forId(id)
            .forFeature(ScheduleFeature.NAME)
            .registerForMessage(STOP_ACTIVITY_ID + id.getName(), SUBJECT, String.class,
                message -> {
                    client.twin().forId(id).retrieve().thenAccept(thing -> {
                        Features features = thing.getFeatures().get();
                        Thing updated;
                        updated = updateProcedureEnd(thing, features.getFeature(SurgeryInfoFeature.NAME).get(), DateTimeType.now());
                        updated = updateRoomInfo(updated,  features.getFeature(RoomInfoFeature.NAME).get());
                        client.twin().update(updated);
                        fireEvent(id, features.getFeature(ScheduleFeature.NAME).get());
                        LOGGER.info("Event fired");
                    });
                    message.reply().statusCode(HttpStatusCode.ACCEPTED).send();
                    LOGGER.info("Operation accepted");
                });
        return true;
    }

    /**
     * Update room info setting operational status to unoccupied and calculating ad updating all index.
     */
    private Thing updateRoomInfo(final Thing thing, final Feature roomFeature) {
        RoomInfoFeature roomInfo = RoomInfoFeature.fromFeature(roomFeature);

        Location loc = fhirClient.read()
            .resource(Location.class)
            .withId(thing.getEntityId().get().getName())
            .encodedJson()
            .execute();
        loc.setOperationalStatus(LocationOperationalStatus.UNOCCUPIED.toConcept().getCodingFirstRep());
        fhirClient.update().resource(loc).execute();
        roomInfo.setOperationalStatus(LocationOperationalStatus.UNOCCUPIED.toCode());

        IndexStrategy lateIndex = new LateIndexStrategy(fhirClient, roomInfo.getReference());
        roomInfo.setLateIndex(lateIndex.calculate());

        IndexStrategy wellIndex = new WellAllocatedIndexStrategy(fhirClient, roomInfo.getReference());
        roomInfo.setWellAllocatedIndex(wellIndex.calculate());

        IndexStrategy postIndex = new PostponedIndexStrategy(fhirClient,
            roomInfo.getReference(), SurgeryAbortReason.NOT_SPECIFIED);
        roomInfo.setPostponedIndex(postIndex.calculate());

        IndexStrategy postBedIndex = new PostponedIndexStrategy(fhirClient,
            roomInfo.getReference(), SurgeryAbortReason.NO_INTENSIVE_BED);
        roomInfo.setPostponedBedIndex(postBedIndex.calculate());
        return thing.setFeatureProperties(RoomInfoFeature.NAME, FeatureProperties.newBuilder()
            .setAll(roomInfo.toJsonObject().stream().collect(Collectors.toList()))
            .build());
    }

    /**
     * This function set end date and update the resource in fhir server.
     */
    private Thing updateProcedureEnd(final Thing thing, final Feature surgeryFeature, final DateTimeType end) {
        SurgeryInfoFeature surgery = SurgeryInfoFeature.fromFeature(surgeryFeature);
        surgery.setStatus(Procedure.ProcedureStatus.COMPLETED.toCode());
        final Reference r = surgery.getReference().get();
        final Procedure procedure = fhirClient.read()
            .resource(Procedure.class)
            .withId(r.getReference())
            .execute();
        final Period period = procedure.getPerformedPeriod().setEndElement(end);
        procedure.setPerformed(period);
        procedure.setStatus(Procedure.ProcedureStatus.COMPLETED);
        fhirClient.update().resource(procedure).execute();
        return thing.setFeatureProperties(SurgeryInfoFeature.NAME, FeatureProperties.newBuilder()
            .setAll(surgery.toJsonObject().stream().collect(Collectors.toList()))
            .build());
    }

    private void fireEvent(ThingId id, Feature scheduleFeature){
        final ScheduleFeature schedule = ScheduleFeature.fromFeature(scheduleFeature);
        SlotEntity slot = schedule.getNow().get();
        surgeryScheduleEvent.fire().activityEndedEvent(id, slot.getReference().getReference());
    }
}
