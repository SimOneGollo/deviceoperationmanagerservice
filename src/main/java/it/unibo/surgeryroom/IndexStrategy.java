package it.unibo.surgeryroom;

import it.unibo.surgeryroom.info.IndexEntity;
import org.eclipse.ditto.model.things.ThingId;

/**
 *  Strategy per il calcolo degli indici.
 * */
public interface IndexStrategy {

    /**
     * Calcola l'indice definito dalla strategia.
     *
     * @return una IndexEntity con il risultato del calcolo effettuato.
     * */
    IndexEntity calculate();
}
