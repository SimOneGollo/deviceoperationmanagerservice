package it.unibo.surgeryroom;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.client.impl.GenericClient;
import org.eclipse.ditto.client.DittoClient;
import org.eclipse.ditto.model.things.ThingId;

import ca.uhn.fhir.rest.client.api.IGenericClient;

public abstract class Operation {

	protected final DittoClient client;
	protected final IGenericClient fhirClient;
	protected final FhirContext fhirContext;
	public Operation(final DittoClient client, final IGenericClient fhirClient) {
		this.client = client;
		this.fhirContext = fhirClient.getFhirContext();
		this.fhirClient = this.fhirContext.newRestfulGenericClient(fhirClient.getServerBase());
	}
	/**
	 * Handle an operation message for thing with id {@code id}.
	 *
	 * @param id of thing to handle operation.
	 *
	 * @return true
	 * */
	public abstract boolean handle(final ThingId id);
}
