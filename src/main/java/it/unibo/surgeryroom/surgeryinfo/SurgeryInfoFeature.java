package it.unibo.surgeryroom.surgeryinfo;

import it.unibo.JsonConvertible;
import org.eclipse.ditto.json.JsonObject;
import org.eclipse.ditto.json.JsonObjectBuilder;
import org.eclipse.ditto.json.JsonValue;
import org.eclipse.ditto.model.things.Feature;
import org.hl7.fhir.r4.model.Reference;

import java.util.Optional;

public final class SurgeryInfoFeature implements JsonConvertible {

    public static final String DEFINITION = "vorto.private.simonegollo.it.unibo:SurgeryInfo:1.0.0";
    public static final String NAME = "surgery";
    public static final String PROPERTY_REF = "/configuration/reference";
    public static final String PROPERTY_STATUS = "/configuration/status";
    public static final String PROPERTY_MOTIVATION_STATUS = "/configuration/statusReason";

    private Reference reference;
    private String status = "";
    private String statusReason = "";

    public SurgeryInfoFeature(String status) {
        this.status = status;
    }

    public static SurgeryInfoFeature fromFeature(Feature f) {
        String status = f.getProperty(PROPERTY_STATUS).orElse(JsonValue.of("unknow")).asString();
        SurgeryInfoFeature sf = new SurgeryInfoFeature(status);

        if (f.getProperty(PROPERTY_REF).isPresent()) {
            sf.setReference(new Reference(f.getProperty(PROPERTY_REF).get().asString()));
        }
        if (f.getProperty(PROPERTY_MOTIVATION_STATUS).isPresent()) {
            sf.setStatusReason(f.getProperty(PROPERTY_MOTIVATION_STATUS).get().asString());
        }
        return sf;
    }

    public Optional<Reference> getReference() {
        return reference == null ? Optional.empty() : Optional.of(reference);
    }

    public void setReference(Reference reference) {
        this.reference = reference;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Optional<String> getStatusReason() {
        return statusReason.equals("") ? Optional.empty() : Optional.of(statusReason);
    }

    public void setStatusReason(String statusReason) {
        this.statusReason = statusReason;
    }

    public JsonObject toJsonObject() {
        JsonObjectBuilder builder = JsonObject.newBuilder()
                .set(PROPERTY_STATUS, getStatus());

        if (getReference().isPresent()) {
            builder.set(PROPERTY_REF, getReference().get().getReference());
        }
        if (getStatusReason().isPresent()) {
            builder.set(PROPERTY_MOTIVATION_STATUS, getStatusReason().get());
        }

        return builder.build();
    }

}
