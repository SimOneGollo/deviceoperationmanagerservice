package it.unibo.surgeryroom;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import it.unibo.BaseService;
import it.unibo.model.DataLoader;
import it.unibo.surgeryroom.schedule.ScheduleFeature;
import it.unibo.surgeryroom.schedule.SlotEntity;
import it.unibo.surgeryroom.schedule.event.SurgeryScheduleEvent;
import it.unibo.utils.FhirUtils;
import org.eclipse.ditto.client.DittoClient;
import org.eclipse.ditto.model.things.ThingId;
import org.hl7.fhir.r4.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class ActivityScheduler extends TimerTask {

    private static final Logger LOGGER = LoggerFactory.getLogger(ActivityScheduler.class);

    private final ThingId id;
    private final DittoClient client;
    private final IGenericClient fhirClient;
    private final SurgeryScheduleEvent surgeryScheduleEvent;
    private final DataLoader loader;

    public ActivityScheduler(FhirContext ctx, DittoClient client, ThingId id) {
        this.id = id;
        this.client = client;
        this.fhirClient = ctx.newRestfulGenericClient(BaseService.CONFIG_PROPERTIES.getFhirEndpointOrThrow());
        this.loader = new DataLoader(ctx.newRestfulGenericClient(fhirClient.getServerBase()));
        this.surgeryScheduleEvent = new SurgeryScheduleEvent(client);
    }

    @Override
    public void run() {
        client.twin().forId(id).forFeature(ScheduleFeature.NAME).retrieve().thenAccept(f -> {
            ScheduleFeature scheduleFeature = ScheduleFeature.fromFeature(f);
            Optional<SlotEntity> sl = nextSlot(scheduleFeature);
            if (sl.isPresent()) {
                LOGGER.info("Next slot is: " + sl.get().getReference().getReference());
                client.twin()
                    .forId(id)
                    .forFeature(ScheduleFeature.NAME)
                    .putProperty(ScheduleFeature.PROPERTY_NOW, sl.get().toJsonObject());
                surgeryScheduleEvent.fire().activityScheduledEvent(
                    id,
                    sl.get().getActivityReference().get().getReference()
                );
            } else {
                LOGGER.info("Next slot not found");
            }
        });
    }

    private Optional<SlotEntity> nextSlot(ScheduleFeature scheduleFeature) {
        DateTimeType next = DateTimeType.now();

        List<Slot> slots = loader.loadSlot(scheduleFeature.getReference());
        LOGGER.info(String.valueOf(slots.size()));
        Optional<Slot> optSlot = slots.stream().filter(s ->
            FhirUtils.dateIsBetween(s.getStart(), next.getValue(), s.getEnd())
        ).findFirst();
        if (optSlot.isPresent()) {
            LOGGER.info(id.getName() + ": " + optSlot.get().getStart());
            SlotEntity sl = new SlotEntity(optSlot.get());
            sl.setActivityReference(getActivityRef(optSlot.get()));
            return Optional.of(sl);
        }
        return Optional.empty();
    }

    private Reference getActivityRef(Slot s) {
        List<Appointment> appointments = new DataLoader(fhirClient).loadAppointmentBySlot(new Reference(s.getIdElement()));
        LOGGER.info(id.getName() + ": " + FhirUtils.relativeReference(appointments.get(0).getIdElement()));
        return FhirUtils.relativeReference(appointments.get(0).getIdElement());
    }

}
