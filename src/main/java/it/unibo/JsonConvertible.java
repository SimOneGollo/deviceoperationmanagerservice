package it.unibo;

import org.eclipse.ditto.json.JsonObject;

public interface JsonConvertible {
    JsonObject toJsonObject();
}
