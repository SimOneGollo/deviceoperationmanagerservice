package it.unibo.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Objects;

public class ResourceLoader {

	public static final String RESOURCE_PATH = ResourceLoader.class.getResource("/").getPath();

	public static String loadModel(String file) {
		String result = "";
		String line = "";
		 try (final BufferedReader reader = new BufferedReader( new InputStreamReader(
				 Objects.requireNonNull(ResourceLoader.class.getClassLoader().getResourceAsStream(file))))) {
            while ((line = reader.readLine()) != null) {
            	//System.out.println(line);
                result += line;
            }
	     } catch (final IOException ioe) {
	    	 throw new IllegalStateException("File " 
			     + file 
			     + " could not be opened but is required for this example: "
			     + ioe.getMessage());
	     }
	     return result;
	}
}
