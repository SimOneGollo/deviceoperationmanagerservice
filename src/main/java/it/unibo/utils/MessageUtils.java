package it.unibo.utils;

import org.eclipse.ditto.client.live.messages.RepliableMessage;
import org.eclipse.ditto.json.JsonFactory;
import org.eclipse.ditto.json.JsonObject;

public class MessageUtils {
	public static boolean isValidPayload(RepliableMessage<JsonObject, Object> message, String... fields) {
		if(message.getPayload().isEmpty()) {
			return false;
		} else {
			boolean flag = true;
			
			for(String field : fields) {
				if(message.getPayload().get().getField(field).isEmpty()) {
					flag = false;
					break;
				}
				
			}
			return flag;
		}
	}
	
	
	public static JsonObject errorResponseMessage(int status, String error, String message, String description, String href) {	
		return JsonFactory.newObject().toBuilder()
			.set("status", status)
			.set("error", error)
			.set("message", message)
			.set("description", description)
			.set("href", href).build();
	}
}
