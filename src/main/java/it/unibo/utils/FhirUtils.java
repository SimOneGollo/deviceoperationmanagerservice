package it.unibo.utils;

import org.hl7.fhir.instance.model.api.IIdType;
import org.hl7.fhir.r4.model.DateTimeType;
import org.hl7.fhir.r4.model.Period;
import org.hl7.fhir.r4.model.Reference;

import java.util.Calendar;
import java.util.Date;

public class FhirUtils {

    public static String[] referenceSplit(final String reference) {
        return reference.split("/");
    }

    public static String referenceToUrl(final String reference) {
        String[] s = referenceSplit(reference);
        return s[0] + "?_id=" + s[1];
    }

    public static Reference relativeReference(final IIdType id) {
        return new Reference(id.getResourceType() + "/" + id.getIdPart());
    }

    public static Period fromDurate(int numberOfDay) {
        DateTimeType end = DateTimeType.today();
        end.add(Calendar.DATE, numberOfDay);
        return new Period()
            .setStartElement(DateTimeType.today())
            .setEndElement(end);
    }

    /**
     * @return true if {@code mid} date is included {@code start} and {@code end}, otherwise return false.
     */
    public static boolean dateIsBetweenOffset(final Date start, final Date mid, final Date end, final long offset) {
        //clone and add offset
        Date offsetStart = (Date) start.clone();
        offsetStart.setTime(offsetStart.getTime() - offset);

        Date offsetEnd = (Date) end.clone();
        offsetEnd.setTime(offsetEnd.getTime() + offset);

        return mid.compareTo(offsetStart) >= 0 &&
            mid.compareTo(offsetEnd) < 0;
    }

    /**
     * @return true if {@code mid} date is included {@code start} and {@code end}, otherwise return false.
     */
    public static boolean dateIsBetween(final Date start, final Date mid, final Date end) {
        return dateIsBetweenOffset(start, mid, end, 0);
    }

    /**
     * @return true if {@code mid} date is included in the {@code period} , otherwise return false.
     */
    public static boolean dateIsBetween(final Period period, final Date mid) {
        return dateIsBetweenOffset(period.getStart(), mid, period.getEnd(), 0);
    }
}
