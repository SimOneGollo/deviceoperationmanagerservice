package it.unibo;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import it.unibo.surgeryroom.Operation;
import it.unibo.surgeryroom.SurgeryDigitalTwinManager;
import it.unibo.surgeryroom.schedule.operation.*;
import it.unibo.utils.DatabaseInitializer;
import org.eclipse.ditto.client.DittoClient;
import org.eclipse.ditto.model.things.ThingId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class MainService extends BaseService {

    private static final String DT_MODEL = "SurgeryModel.json";

    private static final Logger LOGGER = LoggerFactory.getLogger(MainService.class);

    DittoClient serviceClient;
    IGenericClient fhirClient;
    SurgeryDigitalTwinManager surgeryDigitalTwinManager;

    public MainService() {
        serviceClient = newClient();
        startConsumeTwinChanges(serviceClient);
        startConsumeLiveChanges(serviceClient);

        FhirContext ctx = FhirContext.forR4();
        ctx.getRestfulClientFactory().setSocketTimeout(200 * 1000);
        fhirClient = ctx.newRestfulGenericClient(CONFIG_PROPERTIES.getFhirEndpointOrThrow());
    }

    public static List<Class<? extends Operation>> getAllSupportedOperation() {
        List<Class<? extends Operation>> roomDtOp = new ArrayList<>();
        roomDtOp.add(AbortActivityOperation.class);
        roomDtOp.add(StartActivityOperation.class);
        roomDtOp.add(StopActivityOperation.class);
        return roomDtOp;
    }

    public static void main(String[] args) {
        new MainService().start();
    }

    @Override
    public void start() {
        surgeryDigitalTwinManager = new SurgeryDigitalTwinManager(serviceClient, fhirClient);
        if (CONFIG_PROPERTIES.getInitializationFlagOrThrow()) {
            init();
        }

        BaseService.CONFIG_PROPERTIES.getRoomIdOrThrow().forEach(id -> {
            createThingAndHandleOp(
                ThingId.of(BaseService.CONFIG_PROPERTIES.getNamespaceOrThrow(), id),
                MainService.getAllSupportedOperation()
            );
        });
    }

    public void createThingAndHandleOp(ThingId id, List<Class<? extends Operation>> op) {
        surgeryDigitalTwinManager.createThing(id);
        surgeryDigitalTwinManager.handleOperations(id, op);
    }

    @Override
    public void init() {
        DatabaseInitializer dbInit = new DatabaseInitializer(
            fhirClient,
            BaseService.CONFIG_PROPERTIES.getRoomIdOrThrow()
        );
        dbInit.initializeSurgery(BaseService.CONFIG_PROPERTIES.getRoomIdOrThrow());
    }
}






























