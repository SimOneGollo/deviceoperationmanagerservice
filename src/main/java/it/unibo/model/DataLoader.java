package it.unibo.model;

import ca.uhn.fhir.rest.client.api.IGenericClient;
import it.unibo.model.customconcept.LocationOperationalStatus;
import it.unibo.surgeryroom.info.RoomInfoFeature;
import it.unibo.surgeryroom.schedule.ScheduleFeature;
import it.unibo.surgeryroom.schedule.SlotEntity;
import it.unibo.surgeryroom.surgeryinfo.SurgeryInfoFeature;
import it.unibo.utils.FhirUtils;
import it.unibo.utils.ResourceLoader;
import org.eclipse.ditto.model.things.FeatureProperties;
import org.eclipse.ditto.model.things.Thing;
import org.eclipse.ditto.model.things.ThingId;
import org.eclipse.ditto.model.things.ThingsModelFactory;
import org.hl7.fhir.r4.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class DataLoader {
    private static final Logger LOGGER = LoggerFactory.getLogger(DataLoader.class);
    private static final int BUNDLE_MAX_COUNT = 200;

    private final IGenericClient fhirClient;

    public DataLoader(IGenericClient fhirClient) {
        this.fhirClient = fhirClient;//.getFhirContext().newRestfulGenericClient(fhirClient.getServerBase());
    }

    public static DataLoader newLoader(IGenericClient fhirClient) {
        return new DataLoader(fhirClient);
    }

    public static Thing loadThing(ThingId id, String model, IGenericClient fhirClient) {
        Reference _roomRef = new Reference("Location/" + id.getName());
        Thing updated = ThingsModelFactory.newThingBuilder(ResourceLoader.loadModel(model))
            .setId(id).build();

        DataLoader loader = DataLoader.newLoader(fhirClient);
        LOGGER.info("Loader initialized for room: " + id.getName());
        Optional<Schedule> schedule = loader.loadSchedule(_roomRef).stream()
            .filter(s ->
                FhirUtils.dateIsBetween(s.getPlanningHorizon(), DateTimeType.now().getValue())
            ).findFirst();

        if (schedule.isEmpty()) {
            return updated;
        }

        ScheduleFeature scheduleFeature = new ScheduleFeature(
            FhirUtils.relativeReference(schedule.get().getIdElement()));

        Optional<Slot> optionalSlot = loader.loadSlot(scheduleFeature.getReference()).stream()
            .filter(s ->
                FhirUtils.dateIsBetween(s.getStart(), DateTimeType.now().getValue(), s.getEnd())
            ).findFirst();

        SurgeryInfoFeature sif = null;

        if (optionalSlot.isPresent()) {
            SlotEntity slotEntity = new SlotEntity(optionalSlot.get());
            scheduleFeature.setNow(slotEntity);
            List<Appointment> appointments = loader.loadAppointmentBySlot(slotEntity.getReference());

            if (!appointments.isEmpty()) {
                Appointment appointment = appointments.get(0);
                Reference appReference = FhirUtils.relativeReference(appointment.getIdElement());
                Reference appProcedureReference = appointment.getReasonReference().get(0);
                slotEntity.setActivityReference(appReference);
                Procedure procedure = loader.loadProcedureById(appProcedureReference);
                sif = new SurgeryInfoFeature(procedure.getStatus().toCode());
                sif.setReference(FhirUtils.relativeReference(procedure.getIdElement()));

                updated = updated.setFeatureProperties(
                    SurgeryInfoFeature.NAME,
                    FeatureProperties.newBuilder()
                        .setAll(sif.toJsonObject().stream().collect(Collectors.toList()))
                        .build()
                );
            }
        }

        updated = updated.setFeatureProperties(
            ScheduleFeature.NAME,
            FeatureProperties.newBuilder()
                .setAll(scheduleFeature.toJsonObject().stream().collect(Collectors.toList()))
                .build()
        );
        RoomInfoFeature roomInfoFeature = new RoomInfoFeature(_roomRef, id.getName());

        LocationOperationalStatus status = null;
        if (sif != null && sif.getStatus().equals(Procedure.ProcedureStatus.INPROGRESS.toCode())) {
            status = LocationOperationalStatus.OCCUPIED;
        } else {
            status = LocationOperationalStatus.UNOCCUPIED;
        }

        roomInfoFeature.setOperationalStatus(status.toCode());
        updated = updated.setFeatureProperties(
            RoomInfoFeature.NAME,
            FeatureProperties.newBuilder()
                .setAll(roomInfoFeature.toJsonObject().stream().collect(Collectors.toList()))
                .build()
        );

        LOGGER.info("All updated");
        return updated;
    }

    /**
     * Given a {@code locationRef} return all slot referenced by that location
     *
     * @param locationRef surgery room reference
     * @return schedule list.
     */
    public List<Schedule> loadSchedule(Reference locationRef) {

        Bundle b = fhirClient.search()
            .forResource(Schedule.class)
            .where(Schedule.ACTOR.hasId(locationRef.getReference()))
            .count(BUNDLE_MAX_COUNT)
            .returnBundle(Bundle.class)
            .execute();

        if (b.getEntry().isEmpty()) {
            return List.of();
        } else {
            return b.getEntry().stream().map(e -> (Schedule) e.getResource()).collect(Collectors.toList());
        }
    }


    /**
     * Given a {@code scheduleRef} return all slot referenced by that schedule
     *
     * @param scheduleRef schedule given
     * @return found slots.
     */
    public List<Slot> loadSlot(Reference scheduleRef) {
        DateTimeType start = (DateTimeType) DateTimeType.now().setHour(8);
        DateTimeType end = (DateTimeType) start.copy().setHour(20);

        Bundle b = fhirClient.search()
            .forResource(Slot.class)
            .where(Slot.SCHEDULE.hasId(scheduleRef.getReference()))
            .and(Slot.START.exactly().day(DateTimeType.now().getValue()))
            .count(BUNDLE_MAX_COUNT)
            .returnBundle(Bundle.class)
            .execute();

        if (b.getEntry().isEmpty()) {
            return List.of();
        } else {
            return b.getEntry().stream().map(e -> (Slot) e.getResource()).collect(Collectors.toList());
        }
    }

    /**
     * Given a {@code slot} reference return an appointment that occupy that slot.
     *
     * @param slot slot reference used to find a specific appointment.
     * @return found appointments.
     */
    public List<Appointment> loadAppointmentBySlot(Reference slot) {
        Bundle b = fhirClient.search()
            .forResource(Appointment.class)
            .where(Appointment.SLOT.hasId(slot.getReference()))
            .returnBundle(Bundle.class)
            .execute();
        if (b.getEntry().isEmpty()) {
            return List.of();
        } else {
            return b.getEntry().stream().map(e -> (Appointment) e.getResource()).collect(Collectors.toList());
        }
    }

    /**
     * Given a {@code appointmentRef} return an the referenced appointment.
     *
     * @param appointmentRef Appointment reference to find.
     * @return found appointment.
     */
    public Appointment loadAppointment(Reference appointmentRef) {

        String id = FhirUtils.referenceSplit(appointmentRef.getReference())[1];
        LOGGER.info(FhirUtils.referenceSplit(appointmentRef.getReference())[1]);
        return fhirClient.read()
            .resource(Appointment.class)
            .withId(id)
            .encodedJson()
            .execute();
    }

    /**
     * Find all procedure given a {@code locationRef} from first day of moth until
     * current date.
     *
     * @param locationRef surgery room reference.
     * @return list of procedure performed from first day of moth until current date
     */
    public List<Procedure> loadProcedureFromFirstUntilToday(Reference locationRef) {
        DateTimeType dateStart = (DateTimeType) DateTimeType.today().setDay(1);
        DateTimeType dateStop = DateTimeType.today();
        Bundle b = fhirClient.search()
            .forResource(Procedure.class)
            .where(Procedure.LOCATION.hasId(locationRef.getReference()))
            .and(Procedure.DATE.afterOrEquals().day(dateStart.getValue()))
            .and(Procedure.DATE.beforeOrEquals().day(dateStop.getValue()))
            .returnBundle(Bundle.class)
            .count(BUNDLE_MAX_COUNT)
            .execute();
        if (b.getEntry().isEmpty()) {
            return List.of();
        } else {
            return b.getEntry().stream().map(e -> (Procedure) e.getResource()).collect(Collectors.toList());
        }
    }

    /**
     * Find all procedure given a {@code locationRef} performed in current date.
     *
     * @param locationRef surgery room reference.
     * @return list of procedure performed in current date in specified room.
     */
    public List<Procedure> loadProcedureToday(Reference locationRef) {
        Bundle b = fhirClient.search()
            .forResource(Procedure.class)
            .where(Procedure.LOCATION.hasId(locationRef.getReference()))
            .and(Procedure.DATE.exactly().day(DateTimeType.today().getValue()))
            .returnBundle(Bundle.class)
            .execute();
        if (b.getEntry().isEmpty()) {
            return List.of();
        } else {
            return b.getEntry().stream().map(e -> (Procedure) e.getResource()).collect(Collectors.toList());
        }
    }

    public Procedure loadProcedureById(Reference procedure) {
        return fhirClient.read()
            .resource(Procedure.class)
            .withId(procedure.getReference())
            .encodedJson()
            .execute();

    }
}
