package it.unibo.model;

import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import org.hl7.fhir.r4.model.*;

import java.util.List;

import static it.unibo.utils.FhirUtils.relativeReference;

public class DataFactory {


    public static Reference updateResourceRef(IGenericClient fhirClient, Resource r) {
        MethodOutcome result = fhirClient.update()
                .resource(r)
                .encodedJson()
                .execute();
        return relativeReference(result.getId());
    }

    public static Reference newResourceRef(IGenericClient fhirClient, Resource r) {
        MethodOutcome result = fhirClient.create()
                .resource(r)
                .prettyPrint()
                .encodedJson()
                .execute();
        return relativeReference(result.getId());
    }

    public static Appointment newAppointment(List<Reference> slots, List<Reference> participants, Reference reasonReference) {
        Appointment a = new Appointment();
        a.setSlot(slots);
        a.setReasonReference(List.of(reasonReference));
        participants.forEach(p -> {
            Appointment.AppointmentParticipantComponent participant1 = new Appointment.AppointmentParticipantComponent();
            participant1.setActor(p).setStatus(Appointment.ParticipationStatus.ACCEPTED);
            a.addParticipant(participant1);
        });
        return a;
    }

    public static Procedure newProcedure(Reference practitionerRef, Reference location, Reference patientRef) {
        Procedure p = new Procedure();
        Procedure.ProcedurePerformerComponent pc = new Procedure.ProcedurePerformerComponent().setActor(practitionerRef);
        p.setStatus(Procedure.ProcedureStatus.UNKNOWN);
        p.setPerformer(List.of(pc));
        p.setLocation(location);
        p.setSubject(patientRef);
        return p;
    }

    public static Slot newSlot(Reference schedule, InstantType sStart, InstantType sStop) {
        Slot sl = new Slot();
        sl.setSchedule(schedule);
        sl.setStartElement(sStart);
        sl.setEndElement(sStop);
        return sl;
    }

    public static Schedule newSchedule(Reference location, Period planningHorizont) {
        Schedule s = new Schedule();
        s.addActor(location);
        s.setPlanningHorizon(planningHorizont);
        return s;
    }


    public static Location newLocation(String name) {
        Location room1 = new Location();
        room1.setName(name);
        room1.setStatus(Location.LocationStatus.ACTIVE);
        return room1;
    }

    public static Patient newPatient(HumanName name) {
        Patient patient = new Patient();
        return patient.addName(name);
    }

    public static Practitioner newPractitioner(HumanName name) {
        Practitioner practitioner = new Practitioner();
        return practitioner.addName(name);
    }
}
