package it.unibo.model.customconcept;

import org.hl7.fhir.exceptions.FHIRException;
import org.hl7.fhir.r4.model.CodeableConcept;
import org.hl7.fhir.r4.model.Coding;

public enum LocationOperationalStatus {

    CLOSED,
    OCCUPIED,
    UNOCCUPIED;

    public static LocationOperationalStatus fromCode(String codeString) throws FHIRException {
        if (codeString == null || "".equals(codeString))
            return null;
        if ("C".equals(codeString))
            return CLOSED;
        if ("O".equals(codeString))
            return OCCUPIED;
        if ("U".equals(codeString))
            return UNOCCUPIED;
        throw new FHIRException("Unknown LocationOperationalStatus code '" + codeString + "'");
    }

    public String toCode() {
        switch (this) {
            case CLOSED:
                return "C";
            case OCCUPIED:
                return "O";
            case UNOCCUPIED:
                return "U";
            default:
                return "?";
        }

    }

    public String getSystem() {
        return "http://terminology.hl7.org/CodeSystem/v2-0116";
    }

    public String getDefinition() {
        switch (this) {
            case CLOSED:
                return "";
            case OCCUPIED:
                return "";
            case UNOCCUPIED:
                return "";
            default:
                return "?";
        }

    }

    public String getDisplay() {
        switch (this) {
            case CLOSED:
                return "Closed";
            case OCCUPIED:
                return "Occupied";
            case UNOCCUPIED:
                return "Unoccupied";
            default:
                return "?";
        }

    }

    public CodeableConcept toConcept() {
        CodeableConcept c = new CodeableConcept();
        c.addCoding(new Coding(this.getSystem(), this.toCode(), this.getDisplay()));
        c.setText(getDefinition());
        return c;
    }

}
