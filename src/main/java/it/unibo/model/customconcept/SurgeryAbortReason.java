package it.unibo.model.customconcept;

import org.hl7.fhir.exceptions.FHIRException;
import org.hl7.fhir.r4.model.CodeableConcept;
import org.hl7.fhir.r4.model.Coding;

public enum SurgeryAbortReason {
    NO_INTENSIVE_BED,
    NOT_SPECIFIED;

    public static SurgeryAbortReason fromCode(String codeString) throws FHIRException {
        if (codeString == null || "".equals(codeString))
            return null;
        if ("n_i_b".equals(codeString))
            return NO_INTENSIVE_BED;
		if ("no_spec".equals(codeString))
			return NOT_SPECIFIED;
        throw new FHIRException("Unknown AppointmentCancellationReason code '" + codeString + "'");
    }

    public String toCode() {
        switch (this) {
            case NO_INTENSIVE_BED:
                return "n_i_b";
            case NOT_SPECIFIED:
                return "no_spec";
            default:
                return "?";
        }

    }

    public String getSystem() {
        return "it.unibo.model.customconcept";
    }

    public String getDefinition() {
        switch (this) {
            case NO_INTENSIVE_BED:
				return "intensive care bed not available";
			case NOT_SPECIFIED:
				return "no specific reason";
            default:
                return "?";
        }

    }

    public String getDisplay() {
        switch (this) {
            case NO_INTENSIVE_BED:
                return "intensive care bed not available";
			case NOT_SPECIFIED:
				return "no specific reason";
            default:
                return "?";
        }

    }

    public CodeableConcept toConcept() {
        CodeableConcept c = new CodeableConcept();
        c.addCoding(new Coding(this.getSystem(), this.toCode(), this.getDisplay()));
        c.setText(getDefinition());
        return c;
    }
}
