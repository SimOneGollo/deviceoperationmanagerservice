package it.unibo;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import it.unibo.surgeryroom.Operation;
import it.unibo.utils.ResourceLoader;
import org.eclipse.ditto.client.DittoClient;
import org.eclipse.ditto.model.things.Thing;
import org.eclipse.ditto.model.things.ThingId;
import org.eclipse.ditto.model.things.ThingsModelFactory;


public abstract class BaseDigitalTwinManager {

    protected DittoClient client;

    public BaseDigitalTwinManager(DittoClient client) {
        this.client = client;
    }

    protected void createThing(ThingId id) {
        try {
            client.twin().create(id).get(10, TimeUnit.SECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            e.printStackTrace();
        }
    }

    protected void deleteThing(ThingId id) {
        client.twin().forId(id).delete();
    }

    /**
     * handle operation for digital twin {@code id}
     *
     * @param id         of digital twin
     * @param operations to handle
     */
    public abstract BaseDigitalTwinManager handleOperations(ThingId id, List<Class<? extends Operation>> operations);
}
